import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { UserServiceService } from '../data/user-service.service';
import { ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IUser, IContact } from '../data/imodel';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.page.html',
  styleUrls: ['./sign-in.page.scss'],
})
export class SignInPage implements OnInit {
  myForm: FormGroup;
  user: IUser;


  constructor(private toast: Toast, private userSvc: UserServiceService, private formBuilder: FormBuilder,
     public toastController: ToastController,  private router: Router, private nvCrl: NavController) { }

  ngOnInit() {
    this.createForm();
    this.loggedOn();
  }

  createForm() {
    this.myForm = this.formBuilder.group({
      mobileNo: ['', [Validators.required,
        Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(10)
        , Validators.minLength(10)]],
        password: ['', [Validators.required]],
    });
  }
  submitForm () {
    if (!this.myForm.valid) {
      return this.presentToast('Please fill in all the fields');
    }
    this.userSvc.login(this.myForm.value).subscribe(data => {
      localStorage.setItem('token', data['token'] );
      localStorage.setItem('email', this.myForm.value.mobileNo);

      this.userSvc.getUserByEmail(this.myForm.value.mobileNo).subscribe((data: IUser) => {
        this.user = data;
        console.log(this.user.id);
        localStorage.setItem('serviceProviderID',this.user.serviceProviderID);
        localStorage.setItem('username',this.user.firstName);
        this.userSvc.getContactById(this.user.id).subscribe((data:IContact[]) => {
          console.log(data);
          this.userSvc.setContactList(data);
  
        })
        localStorage.setItem('userId', this.user.id);
      });
      if(localStorage.getItem('userId') !== null){

    }
       this.router.navigate(['/home-page']);

      console.log(data);
    }, error =>{
      return this.presentToast(error.error);
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
  gotoMenu() {
    this.router.navigate(['/tab1']);
     }
loggedOn() {
  if (localStorage.getItem('token') !== null) {
    this.nvCrl.navigateRoot('/home-page');
  }
}
facebook(){
  window.open('https://www.facebook.com/', '_system');
}

}
