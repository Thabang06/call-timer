import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { UpdateContactPage } from './update-contact.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateContactPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes),
    ReactiveFormsModule,

  ],
  declarations: [UpdateContactPage]
})
export class UpdateContactPageModule {}
