import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { UserServiceService } from '../data/user-service.service';
import { ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IContact } from '../data/imodel';

@Component({
  selector: 'app-update-contact',
  templateUrl: './update-contact.page.html',
  styleUrls: ['./update-contact.page.scss'],
})
export class UpdateContactPage implements OnInit {
  myForm: FormGroup;
  contact: IContact
  constructor(private toast: Toast, private userSvc: UserServiceService,
    private formBuilder: FormBuilder,  public toastController: ToastController, private router: Router, private nvCrl: NavController) { }

  ngOnInit() {
    this.createForm();
    this.getContacts();
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [''],
      userId: [''],
      contactName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)]],
      contactNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(10)
        , Validators.minLength(10)]]
    });
  }
  submitForm() {
    if (!this.myForm.valid) {
      return this.presentToast('Please fill in all the fields');
    }

    this.userSvc.saveContact(this.myForm.value).subscribe((data: IContact) => {
      console.log(data);
      this.userSvc.getContactById(localStorage.getItem('userId')).subscribe((data:IContact[]) => {
        console.log(data);
        
        this.userSvc.clearContactList();
        this.userSvc.setContactList(data);
      })
      this.presentToast('Contact Successfully Updated!');
      console.log(data.id);
      localStorage.setItem('contactId', data.id);
      this.getContact();
      this.nvCrl.navigateRoot('/view-contact');
    }, error => {
      this.presentToast('Error while saving!');
    
    });
  }
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
  getContacts() {
    console.log(localStorage.getItem('contactId'));

    this.userSvc.getContactId(localStorage.getItem('contactId')).subscribe( (data: IContact) => {
      console.log(data);
      this.contact = data;
      this.loadDataFromDatabase(data);
    })
  }
  loadDataFromDatabase(contact: IContact) {
    console.log(contact);
    this.myForm.patchValue({
                              id: contact.id,
                              userId: contact.userId,
                              contactName: contact.contactName,
                              contactNumber: contact.contactNumber,
    });
  }

  getContact() {
    this.userSvc.getContactId(localStorage.getItem('contactId')).subscribe( (data: IContact) => {
      console.log(data);
      this.userSvc.setUpdatedContact(data);
    })
  }

}
