import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../data/user-service.service';
import { IContact } from '../data/imodel';
import { Router } from '@angular/router';
import { ToastController, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-view-contact',
  templateUrl: './view-contact.page.html',
  styleUrls: ['./view-contact.page.scss'],
})
export class ViewContactPage implements OnInit {
  contact: IContact;
  contactName
  contactNumber

  constructor(private userSvc: UserServiceService, private router: Router, private toastController: ToastController,private alertCtrl:AlertController) { }

  ngOnInit() {
    this.getContact();
  }

  getContact() {
    console.log(localStorage.getItem('contactId'));

    this.userSvc.getContactId(localStorage.getItem('contactId')).subscribe( (data: IContact) => {
      console.log(data);
      this.contact = data;
      this.contactName = data.contactName;
      this.contactNumber = data.contactNumber;
    });

    this.userSvc.getUpdatedContact().subscribe(data => {
      console.log(data);
      this.contact = data;
      this.contactName = data.contactName;
      this.contactNumber = data.contactNumber;
    })
  }

  getUpdatedContacts() {
    console.log(localStorage.getItem('userId'));
      this.userSvc.getContactById(localStorage.getItem('userId')).subscribe((data:IContact[]) => {
        console.log(data);
        this.userSvc.setContactList(data);

      })
      this.userSvc.getContactList().subscribe( data => {
        console.log(data);
        // this.contactlist = data;
      })
    

  }

  updateContact(id) {
    localStorage.setItem('contactId', id);
    this.router.navigate(['/update-contact']);
  }

  deleteContact(id){
    console.log(id);
    this.userSvc.deleteContact(id).subscribe(x => {
      console.log(x);
      this.getUpdatedContacts();
      this.presentToast('Contact Successfully Deleted');
      this.router.navigate(['/home-page']);
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
  async  showConfirmAlert(i) {
    console.log(i);

    const alert = await  this.alertCtrl.create({
      header: 'Confirm delete contact',
        message: 'Are you sure you want to permanently delete this contact?',
        buttons: [
            {
                text: 'No',
                handler: () => {
                    console.log('Cancel clicked');
                }
            },
            {
                text: 'Yes',
                handler: () => {
                    this.deleteContact(i);
                }
            }
        ]
    });
    await alert.present();

  }
}
