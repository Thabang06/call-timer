import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../data/user-service.service';
import { ICallLog } from '../data/imodel';

@Component({
  selector: 'app-call-log',
  templateUrl: './call-log.page.html',
  styleUrls: ['./call-log.page.scss'],
})
export class CallLogPage implements OnInit {
calls: ICallLog[];
  constructor( private userSvc: UserServiceService) { }

  ngOnInit() {
    this.callLog();
  }
callLog(){
  this.userSvc.getCallLog(localStorage.getItem("userId")).subscribe(x => {
    console.log(x)
    this.calls = x; 
  })
}
}
