import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../data/user-service.service';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { NavController, ToastController } from '@ionic/angular';
import { TabsPageModule } from '../tabs/tabs.module';
import { Router } from '@angular/router';
import { IServiceProvider } from '../data/imodel';

@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.page.html',
  styleUrls: ['./sign-up.page.scss'],
})
export class SignUpPage implements OnInit {
  myForm: FormGroup;
  serviceProviders: IServiceProvider;
  data: string;
date: Date = new Date();
  constructor(private toast: Toast, private userSvc: UserServiceService,
    private formBuilder: FormBuilder,  public toastController: ToastController, private router: Router) { }

  ngOnInit() {
    this.createForm();
    this.getServiceProvider();
  }

  createForm() {
    this.myForm = this.formBuilder.group({
      firstName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)
      , Validators.minLength(2)]],
      idNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(13)
        , Validators.minLength(13)]],
      email: ['', [Validators.required,
        Validators.pattern(/\S+@\S+\.\S+/)]],
      mobileNo: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(10)
        , Validators.minLength(10)]],
        serviceProviderID: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
        password: ['', [Validators.required]],
    });
  }

  submitForm() {
    if (!this.myForm.valid) {
      return this.presentToast('Please fill in the correct fields');
    }
    if (this.myForm.value.confirmPassword !== this.myForm.value.password) {
      return this.presentToast('Passwords are not the same');
    }
    this.data = this.myForm.value.mobileNo.toString().substring(0,2);
    if(this.data === '01' || this.data === '02' || this.data === '03' || this.data === '04' || this.data === '05') {
      return this.presentToast('Please enter a valid mobile number');
    }
    if(this.myForm.value.idNumber.toString().substring(0,2) === this.date.getFullYear().toString().substring(2,4)){
      return this.presentToast('Please enter a valid Id number');

    }
    
    console.log(this.myForm.value.idNumber.toString().substring(0,2));

    this.userSvc.saveUser(this.myForm.value).subscribe(data => {
      localStorage.setItem('Id', data);
      this.presentToast('Your details have been saved!');
      this.router.navigate(['/sign-in']);
    }, error => {
      this.presentToast(error.error.text);

    });
  }
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
  getServiceProvider() {
    this.userSvc.getServiceProvider().subscribe(data => {
      console.log(data);
      this.serviceProviders = data;
    })
  }

}
