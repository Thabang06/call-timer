import { Component, OnInit, ViewChild } from '@angular/core';
import { MenuController, IonInfiniteScroll, ModalController, Platform } from '@ionic/angular';
import { Contacts, Contact, ContactField, ContactName } from '@ionic-native/contacts';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { CallDurationPage } from '../call-duration/call-duration.page';
import { Router } from '@angular/router';
import { IUser, IContact, ICallLog, ICallReport } from '../data/imodel';
import { UserServiceService } from '../data/user-service.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { NativeAudio } from '@ionic-native/native-audio/ngx';
import { TextToSpeech } from '@ionic-native/text-to-speech/ngx';
import { CallLog } from '../data/model';

declare var cordova;
@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.page.html',
  styleUrls: ['./home-page.page.scss'],
})
export class HomePagePage implements OnInit {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;
  private contactlist: IContact[];
  private min: any;
  user: IUser;
  userContacts: any;
  searchTerm: string = '';
  allContacts: IContact[];
  phone: any;
  username: string;
  handler: string;
callLog:ICallReport;
noResults: boolean = false;


// tslint:disable-next-line: deprecation
  constructor(private modalCtrl:ModalController, private menu: MenuController, private contacts: Contacts, private callNumber: CallNumber,
    private router: Router, private userSvc: UserServiceService,private platform:Platform,private camera: Camera, private nativeAudio: NativeAudio,
    private tts: TextToSpeech) { }
  ngOnInit() {
   // this.ringtone();
    

    this.getUser();
     this.getContacts();
    //this.loadContacts();
    // this.contacts.find(['displayName', 'phoneNumbers']).then((contacts) => {
    //   this.contactlist = contacts;
    //   console.log(this.contactlist);
    //   for (let i = 0; i < 25; i++) {
    //     this.contactlist.push(this.contactlist[i]);
    //   }
    // });
  }
  // loadContacts(){
  //   this.userSvc.getDatabaseState().subscribe(isReady => {
  //     console.log(isReady);
  //     if(isReady)
  //     {
  //       this.getContacts();
  //     }
  //   })
  // }
  openFirst() {
    this.menu.enable(true, 'first');
    this.menu.open('first');
  }
  //   loadData(event) {

  //   setTimeout(() => {
  //     console.log('Done');
  //     for (let i = 0; i < 25; i++) {
  //       this.contactlist.push(this.contactlist[i]);
  //     }
  //     event.target.complete();

  //     // App logic to determine if all data is loaded
  //     // and disable the infinite scroll
  //     if (this.contactlist.length === 50) {
  //       event.target.disabled = true;
  //     }
  //   }, 500);
  // }

  // toggleInfiniteScroll() {
  //   this.infiniteScroll.disabled = !this.infiniteScroll.disabled;
  // }
  async CallDurationPage()
  {
    const modal = await this.modalCtrl.create({
     component: CallDurationPage
   });
   modal.onDidDismiss().then((object) => {
    console.log(object.data);
    this.min = object.data;
    console.log(this.phone);
    this.callLog = {
      userId: localStorage.getItem("userId"),
      callFrom : localStorage.getItem("email"),
      callTo: this.phone,
      callLimit: this.min
    }
    console.log(this.callLog);
    if(this.min>0){

      this.callNumber.callNumber(this.phone, false)
      .then(res =>{
        if(res){
          this.timer();
          this.userSvc.saveCallLog(this.callLog).subscribe((data) =>{
            console.log(data);
          });
        }
      })
      .catch(err => console.log('Error launching dialer', err));

    }
    

  });

   return await modal.present();
  }
  
timer(){
  this.min = (this.min * 1000) + 10000;
  console.log(this.min);
  setTimeout(() =>{
 this.ringtone();
  },this.min);

  
}
  Call(event) {
    console.log(event)
    this.phone = event;
     this.CallDurationPage();
    // if(this.min > 0){
      
    //   }
    }
    makeCall(){
      // this.callNumber.callNumber('*111#', true)
      //   .then(res =>{
      //      console.log(res);
      //      console.log('*111#');
      //   })
      //   .catch(err => console.log('Error launching dialer', err))

        this.platform.ready().then(() =>{
          var x = cordova.plugins.CordovaCall.endCall(this.phone);
          console.log(x);
        })
        
    }
cameraCap()
{
  const options: CameraOptions = {
    quality: 100,
    destinationType: this.camera.DestinationType.FILE_URI,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE
  }
  
  this.camera.getPicture(options).then((imageData) => {
   // imageData is either a base64 encoded string or a file URI
   // If it's base64 (DATA_URL):
   let base64Image = 'data:image/jpeg;base64,' + imageData;
  }, (err) => {
   // Handle error
  });
}
    goToContact() {
      this.router.navigate(['/contact']);
    }
    goToUpateContact() {
      this.router.navigate(['/update-contact']);
    }


    getContacts() {
      console.log(localStorage.getItem('userId'));
        this.userSvc.getContactById(localStorage.getItem('userId')).subscribe((data:IContact[]) => {
          console.log(data);
          this.userSvc.setContactList(data);
  
        })
        this.userSvc.getContactList().subscribe( data => {
          console.log(data);
          this.contactlist = data;
        })
      

    }
    // UpdateContact(id){
    //   localStorage.setItem('contactId', id);
    //   this.goToUpateContact();
    // }
    viewContact(id) {
      localStorage.setItem('contactId', id);
      this.router.navigate(['/view-contact']);
    }
    // DeleteContact(id){
    //   console.log(id);
    //   this.userSvc.deleteContact(id).subscribe(x => {
    //     console.log(x);
    //     this.getContacts();
    //   });
    // }
    getUser(){
      this.userSvc.getUserByEmail(localStorage.getItem('email')).subscribe((data: IUser) => {
        this.user = data;
        console.log(this.user.id);
        // this.username = data.firstName;
        this.username = localStorage.getItem('username');
        this.userSvc.getContactById(this.user.id).subscribe((data:IContact[]) => {
          console.log(data);
          this.userSvc.setContactList(data);
    
        })
    })
  }
     searchContact() {
       // this.searchTerm = ev.target.value;
       console.log(this.searchTerm);
       this.userSvc.getContactById(localStorage.getItem('userId')).subscribe( (data:IContact[]) => {
        console.log(data);
        this.contactlist = data;
        this.allContacts = data;
        this.noResults = false;
        if(!this.searchTerm){
          console.log(this.allContacts);
          return this.contactlist = this.allContacts;
        }
        this.contactlist = this.allContacts.filter((item) => {
                 return item.contactName.toLowerCase().indexOf(this.searchTerm.toLowerCase()) > -1;
            });
            if(this.contactlist.length === 0){
              console.log("Success");
             this.noResults = true;
     
            }
      })
   }

   viewBalance(){
    this.userSvc.getServiceProviderById(localStorage.getItem('serviceProviderID')).subscribe(data => {
      console.log(data.serviceProviderUSSD);
      this.callNumber.callNumber(data.serviceProviderUSSD, true)
      .then(res =>{
         console.log(res);
      })
      .catch(err => console.log('Error launching dialer', err))
    })
    }

    ringtone(){
      // this.platform.ready().then(() => {
      // this.nativeAudio.preloadSimple('phoneRing', 'assets/ringtones/phone_ring.mp3').then((x)=>{
      //   console.log(x);
      //   this.nativeAudio.play('phoneRing');
    
      // });
      // });
      this.tts.speak('Hello '+this.username+', you have exceeded your call limit. If you continue with the call, you may deplete your minutes. Please end the call now.')
  .then(() => console.log('Success'))
  .catch((reason: any) => console.log(reason));
    
    }
}
