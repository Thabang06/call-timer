import { Injectable } from '@angular/core';
import { IUser, ILogin, IServiceProvider, IContact, ICallLog, IForgotPassword, ICallReport } from './imodel';
import { environment } from 'src/environments/environment';
import { Observable, BehaviorSubject, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';



@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  private apiUrl = environment.apiUrl;
  private _registerController = 'register';
  private _login = 'register/login';
  private _email = 'register/email';
  private _delete = 'Register/Delete/';
  private _serviceProvider = 'serviceProvider';
  private _serviceProviderById = 'serviceProviderById';
  private _contact = 'contact';
  private _userContact = 'getContact';
  private _contactDelete = 'Contact/Delete/';
  private _callLog = 'CallLog';
  private _callLogID = '/CallLogID/'
  private _generatePasswordCode = '/generatePasswordCode';
  private _resetPassword = '/resetPassword';

  private databaseReady: BehaviorSubject<boolean>;
  private contactlist = new Subject<IContact[]>();
  private UpdatedContact = new Subject<IContact>();
  private userEmail = new  BehaviorSubject<string>('');


  constructor(private httpClient: HttpClient) {
    this.databaseReady = new BehaviorSubject(false);
   }

  saveUser(user: IUser): Observable<string> {
    return this.httpClient.post<string>(this.apiUrl + this._registerController, user);
  }
  login(login: ILogin) {
    return this.httpClient.post(this.apiUrl + this._login, login);
  }
  getCurrentUser(userid: string): Observable<IUser> {
    return this.httpClient.get<IUser>(this.apiUrl + this._registerController + '?id=' + userid);
  }
  getUserByEmail(email: string): Observable<IUser> {
    return this.httpClient.get<IUser>(this.apiUrl + this._email + '/' + email);
  }
  delete(email: string) {
    return this.httpClient.delete(this.apiUrl + this._delete + email);

  }
  getServiceProvider() {
    return this.httpClient.get<IServiceProvider>(this.apiUrl + this._serviceProvider);
  }
  getServiceProviderById(id: string) {
    return this.httpClient.get<IServiceProvider>(this.apiUrl + this._serviceProvider + '/' + this._serviceProviderById + '/' + id);
  }
  saveContact(contact: IContact): Observable<IContact> {
    this.databaseReady.next(true);
    return this.httpClient.post<IContact>(this.apiUrl + this._contact, contact);
  }
  getContactById(id: string) {
  //  this.databaseReady.next(true);
    return this.httpClient.get(this.apiUrl + this._contact + '/' + this._userContact + '?id=' + id);

  }
  getContactId(id: string) {
    //  this.databaseReady.next(true);
      return this.httpClient.get(this.apiUrl + this._contact + '?id=' + id);
  
    }
  getDatabaseState() {
    return this.databaseReady.asObservable();
  }

  setContactList(data: IContact[]) {
    this.contactlist.next(data);
    console.log(data)
  }

  getContactList() : Observable<IContact[]> {
    return this.contactlist.asObservable();
  }
  
  clearContactList() {
    this.contactlist.next();
  }

  deleteContact(id: string) {
    return this.httpClient.delete(this.apiUrl + this._contactDelete + id);
  }

  setUpdatedContact(data: IContact) {
    this.UpdatedContact.next(data);
    console.log(data)
  }

  getUpdatedContact() : Observable<IContact> {
    return this.UpdatedContact.asObservable();
  }
  saveCallLog(callLog: ICallReport): Observable<IContact> {
    return this.httpClient.post<IContact>(this.apiUrl + this._callLog, callLog);
  }

  generatePasswordCode(data: IForgotPassword): Observable<IForgotPassword> {
    return this.httpClient.post<IForgotPassword>(this.apiUrl + this._registerController + this._generatePasswordCode, data);
  }

  resetPassword(data: IForgotPassword): Observable<IForgotPassword> {
    return this.httpClient.post<IForgotPassword>(this.apiUrl + this._registerController + this._resetPassword, data);
  }

  setUserEmail(data: string) {
    this.userEmail.next(data);
  }

  getUserEmail(): Observable<string> {
    return this.userEmail.asObservable();
  }

  clearUserEmail() {
    this.userEmail.next('');
  }
  getCallLog(id){
    return this.httpClient.get<ICallLog[]>(this.apiUrl + this._callLog + this._callLogID + id);
  }
}
