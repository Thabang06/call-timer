import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { UserServiceService } from '../data/user-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IForgotPassword } from '../data/imodel';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.page.html',
  styleUrls: ['./reset-password.page.scss'],
})
export class ResetPasswordPage implements OnInit {
  myForm: FormGroup;
  forgotPassword: IForgotPassword;
  userEmali = '';

  constructor(private toast: Toast, private userSvc: UserServiceService,
    private formBuilder: FormBuilder,  public toastController: ToastController, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.myForm = this.formBuilder.group({
      passwordResetCode: ['', [Validators.required]],
        confirmPassword: ['', [Validators.required]],
        newPassword: ['', [Validators.required]]
    });
  }

  submitForm() {
    if (!this.myForm.valid) {
      return this.presentToast('Please fill in all the fields');
    }
    if (this.myForm.value.confirmPassword !== this.myForm.value.newPassword) {
      return this.presentToast('Passwords are not the same');
    }
    
    this.userSvc.getUserEmail().subscribe(data => {
      console.log(data);
      this.myForm.value.email = data;
    })
    this.forgotPassword = this.myForm.value;
    console.log(this.forgotPassword);
    this.userSvc.resetPassword(this.forgotPassword).subscribe(data => {
      console.log(data);
      this.presentToast('Your password has been successfully reset.');
      this.router.navigate(['/sign-in']);
    }, error => {
      this.presentToast(error.error.text);

    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }

  resendCode() {
    this.forgotPassword.email = this.userSvc.getUserEmail().toString();

    this.userSvc.generatePasswordCode(this.forgotPassword).subscribe(data => {
      console.log(data);
      this.presentToast('Code sent succcessfully.');
    }, error => {
      this.presentToast(error.error.text);
    })
  }

}
