import { Component } from '@angular/core';

import { Platform, MenuController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Router } from '@angular/router';
import { UserServiceService } from './data/user-service.service';
import { CallNumber } from '@ionic-native/call-number/ngx';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private router: Router, private menuCtrl: MenuController,
    private userSvc: UserServiceService,
    private callNumber: CallNumber
  ) {
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {


      // CallTrap.onCall((state) => {
      //   console.log(`CHANGE STATE: ${state}`)
       
      //   switch (state) {
      //     case CallTrap.STATE.RINGING:
      //       console.log('Phone is ringing')
      //       break
      //     case CallTrap.STATE.OFFHOOK:
      //       console.log('Phone is off-hook')
      //       break
      //     case CallTrap.STATE.IDLE:
      //       console.log('Phone is idle')
      //       break
      //   }
      // });

      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  viewProfile() {
    this.router.navigate(['/profile']);
    this.menuCtrl.close();
  }
  viewCallLog(){
    this.router.navigate(['call-log']);
    this.menuCtrl.close();
  }
viewBalance(){
this.userSvc.getServiceProviderById(localStorage.getItem('serviceProviderID')).subscribe(data => {
  console.log(data.serviceProviderUSSD);
  this.callNumber.callNumber(data.serviceProviderUSSD, true)
  .then(res =>{
     console.log(res);
  })
  .catch(err => console.log('Error launching dialer', err))
})
}
  logout() {
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    localStorage.removeItem('Id');
    localStorage.removeItem('contactId');
    localStorage.removeItem('userId');
    localStorage.removeItem('username');
    this.menuCtrl.close();
    this.router.navigate(['/sign-in']);
    this.userSvc.clearContactList();
  }
}
