import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
   //{ path: '', loadChildren: './home-page/home-page.module#HomePagePageModule' },
  { path: '', loadChildren: './sign-in/sign-in.module#SignInPageModule' },
  { path: 'tabs', loadChildren: './tabs/tabs.module#TabsPageModule' },
  { path: 'sign-in', loadChildren: './sign-in/sign-in.module#SignInPageModule' },
  { path: 'sign-up', loadChildren: './sign-up/sign-up.module#SignUpPageModule' },
  { path: 'home-page', loadChildren: './home-page/home-page.module#HomePagePageModule' },
  {path: 'tab1', loadChildren: './tab1/tab1.module#Tab1PageModule' },
  {path: 'tab2', loadChildren: './tab2/tab2.module#Tab2PageModule' },
  {path: 'tab3', loadChildren: './tab3/tab3.module#Tab3PageModule' },
  { path: 'profile', loadChildren: './profile/profile.module#ProfilePageModule' },
  { path: 'call-duration', loadChildren: './call-duration/call-duration.module#CallDurationPageModule' },
  { path: 'contact', loadChildren: './contact/contact.module#ContactPageModule' },
  { path: 'update-contact', loadChildren: './update-contact/update-contact.module#UpdateContactPageModule' },  { path: 'view-contact', loadChildren: './view-contact/view-contact.module#ViewContactPageModule' },
  { path: 'generate-code', loadChildren: './generate-code/generate-code.module#GenerateCodePageModule' },
  { path: 'reset-password', loadChildren: './reset-password/reset-password.module#ResetPasswordPageModule' },
  { path: 'call-log', loadChildren: './call-log/call-log.module#CallLogPageModule' }





 


];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules }),
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
