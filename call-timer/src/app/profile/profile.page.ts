import { Component, OnInit } from '@angular/core';
import { UserServiceService } from '../data/user-service.service';
import { IUser, IServiceProvider } from '../data/imodel';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { Toast } from '@ionic-native/toast/ngx';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
user: IUser;
myForm: FormGroup;
serviceProviders: IServiceProvider;


  constructor(private userSvc: UserServiceService, private formBuilder: FormBuilder,
     public toastController: ToastController, private router: Router, private toast: Toast) { }

  ngOnInit() {
    this.createForm();
    this.currentUser();
    this.getServiceProvider();
  }
  createForm() {
    this.myForm = this.formBuilder.group({
      id: [''],
      firstName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)]],
      lastName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)]],
      email: ['', [Validators.required,
        Validators.pattern(/^[a-zA-Z0-9.!#$%&’*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/)]],
      mobileNo: ['', [Validators.required, Validators.maxLength(10)
        , Validators.minLength(10)]],
        serviceProviderID: ['', [Validators.required]]
    });
  }
currentUser() {
  console.log(localStorage.getItem('email'));
  if (localStorage.getItem('Id') == null) {
    this.userSvc.getUserByEmail(localStorage.getItem('email')).subscribe((data: IUser) => {
      this.user = data;
      console.log(this.user);
      this.loadDataFromDatabase(this.user);
});
    return false;
  }
  this.userSvc.getCurrentUser(localStorage.getItem('Id')).subscribe((data: IUser) => {
        this.user = data;
        console.log(this.user);
        this.loadDataFromDatabase(this.user);
  });
}

loadDataFromDatabase(user: IUser) {
  console.log(user);
  this.myForm.patchValue({
                            id: user.id,
                            firstName: user.firstName,
                            lastName: user.lastName,
                            email: user.email,
                            mobileNo: user.mobileNo,
                            serviceProviderID: user.serviceProviderID
  });
}

submitForm() {
  if (!this.myForm.valid) {
    return this.presentToast('Please fill in all the fields');
  }
  this.userSvc.saveUser(this.myForm.value).subscribe(data => {
    this.presentToast('Your details have been updated!');
    this.router.navigate(['/home-page']);
  });
}
async presentToast(message: string) {
  const toast = await this.toastController.create({
    message: message,
    duration: 5000,
    showCloseButton: true,
    closeButtonText: 'Close'
  });
  toast.present();
}
deleteAccount() {
  this.userSvc.delete(localStorage.getItem('email')).subscribe(data => {
    this.presentToast('Your account has been deleted!');
    localStorage.removeItem('email');
    localStorage.removeItem('token');
    localStorage.removeItem('Id');
    this.router.navigate(['/sign-in']);
  });
}
getServiceProvider() {
  this.userSvc.getServiceProvider().subscribe(data => {
    console.log(data);
    this.serviceProviders = data;
  })
}
}
