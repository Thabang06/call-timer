import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { UserServiceService } from '../data/user-service.service';
import { ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IForgotPassword } from '../data/imodel';

@Component({
  selector: 'app-generate-code',
  templateUrl: './generate-code.page.html',
  styleUrls: ['./generate-code.page.scss'],
})
export class GenerateCodePage implements OnInit {
  myForm: FormGroup;
  forgotPassword: IForgotPassword;

  constructor(private toast: Toast, private userSvc: UserServiceService,
    private formBuilder: FormBuilder,  public toastController: ToastController, private router: Router) { }

  ngOnInit() {
    this.createForm();
  }


  createForm() {
    this.myForm = this.formBuilder.group({
      
      email: ['', [Validators.required,
        Validators.pattern(/\S+@\S+\.\S+/)]],
    });
  }

  submitForm() {
    if (!this.myForm.valid) {
      return this.presentToast('Please enter a valid email address');
    }
    this.forgotPassword = this.myForm.value;
    console.log(this.forgotPassword);
    this.userSvc.generatePasswordCode(this.forgotPassword).subscribe(data => {
      console.log(data);
      // localStorage.setItem('Id', data);
      this.userSvc.setUserEmail(data.email);
      this.presentToast('We have sent you a code, please check your email');
      this.router.navigate(['/reset-password']);
    }, error => {
      this.presentToast(error.error.text);

    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
}
