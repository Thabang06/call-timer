import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ModalController, ToastController } from '@ionic/angular';

@Component({
  selector: 'app-call-duration',
  templateUrl: './call-duration.page.html',
  styleUrls: ['./call-duration.page.scss'],
})
export class CallDurationPage implements OnInit {
  myFormInfo: FormGroup;
  constructor(private formBuilder: FormBuilder,private modalCtrl:ModalController, private toastController: ToastController) { }

  ngOnInit() {
    this.createForm();
  }
  createForm() {
    this.myFormInfo = this.formBuilder.group({
      duration: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/)]],
    });
  }
  submitForm(){
    if(!this.myFormInfo.valid) {
      return this.presentToast('Please fill in all the fields');
    }
    this.modalCtrl.dismiss(this.myFormInfo.value.duration);
  }
close(){
  this.modalCtrl.dismiss();

}
  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
}
