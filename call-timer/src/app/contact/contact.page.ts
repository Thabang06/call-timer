import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { Toast } from '@ionic-native/toast/ngx';
import { UserServiceService } from '../data/user-service.service';
import { ToastController, NavController } from '@ionic/angular';
import { Router } from '@angular/router';
import { IContact } from '../data/imodel';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  myForm: FormGroup;

  constructor(private toast: Toast, private userSvc: UserServiceService,
    private formBuilder: FormBuilder,  public toastController: ToastController, private router: Router, private nvCrl: NavController) { }

  ngOnInit() {
    this.createForm();
  }

  createForm() {
    this.myForm = this.formBuilder.group({
      userId: [''],
      contactName: ['', [Validators.required, Validators.pattern(/^(?=.*[a-zA-Z])[a-zA-Z]+$/)]],
      contactNumber: ['', [Validators.required, Validators.pattern(/^(?=.*[0-9])[0-9]+$/), Validators.maxLength(10)
        , Validators.minLength(10)]]
    });
  }

  submitForm() {
    if (!this.myForm.valid) {
      return this.presentToast('Please fill in all the fields');
    }
    this.myForm.value.userId = localStorage.getItem('userId');
    this.userSvc.saveContact(this.myForm.value).subscribe(data => {
      console.log(data);
      // this.userSvc.getContactById(data);
      this.presentToast('Contact Successfully Created!');
      this.userSvc.getContactById(localStorage.getItem('userId')).subscribe((data:IContact[]) => {
        console.log(data);
        this.userSvc.clearContactList();
        this.userSvc.setContactList(data);
      })
      this.nvCrl.navigateRoot('/home-page');
    }, error => {
      this.presentToast('Error while saving!');
      console.log(error);
    
    });
  }

  async presentToast(message: string) {
    const toast = await this.toastController.create({
      message: message,
      duration: 5000,
      showCloseButton: true,
      closeButtonText: 'Close'
    });
    toast.present();
  }
}
